$(function(){
    
    // $("#add_another_phone_no").click(function(){
    //     var id = 2;
    //        $(".phone_wrapper").append(
   
    //            `
    //            <div class="form-row">
    //            <div class="col-md-7">
    //            <label for="address_name">Mobile Number</label>  
    //            </div>
    //            <div class="col-md-5">
    //            <input type="radio" id="primary_phone_${id}"   name="primary_phone" >Primary
    //            </div>
               
    //        </div>
           
     
    //          <input  type="text" 
    //                name="phone_no[]" 
    //                id="phone_no" 
    //                class="form-control <?= $errors!='' && $errors->has('phone_no') ? 'error' : '';?>"
    //                placeholder = "phone_no"
    //                value=""/>
    //          <br>
    //            `
    //        )
    //        id++;
    //    }),
   
       $("#add_another_whatapp_no").click(function(){
           $(".whatapp_wrapper").append(
   
               `
               <div class = "form-group">
               <input  type="text" 
               name="whatsapp_no" 
               id="whatsapp_no" 
               class="form-control <?= $errors!='' && $errors->has('whatsapp_no') ? 'error' : '';?>"
               placeholder = "whatapp_no"
               value=""/>
               </div>
               `
           )
          
       }),
   
       $("#add_another_Email").click(function(){
           $(".email_wrapper").append(
   
               `
               <div class = "form-group">
               <input  type="text" 
               name="email" 
               id="email" 
               class="form-control <?= $errors!='' && $errors->has('email') ? 'error' : '';?>"
               placeholder = "email"
               value=""/>
               </div>
               `
           )
           
       })
      
       $("#add-employee").validate({
           'rules': {
               'first_name':{
                   required: true,
                   minlength: 2,
                   maxlength: 255
               },
               'last_name':{
                   required: true,
                   minlength: 2,
                   maxlength: 255
               },
               'phone_no':{
                   required: true,
                   minlength: 10,
                   maxlength: 10
               },
               'mail':{
                   required: true,
                   email: true
               },
               'address1':{
                   required: true,
                   minlength: 2,
                   maxlength: 255
               },
               'address2':{
                   required: false,
                   minlength: 2,
                   maxlength: 255
               },
               'location':{
                   required: false,
                   minlength: 2,
                   maxlength: 255
               },
               
               
               
   
               
      
           },
   
           
    
           submitHandler: function(form) {
               // do other things for a valid form
               form.submit();
           }
       });
   });
   
var id = 2;
function addPhone()
{
 
    $(".phone_wrapper").append(
   
        `
        <div class="form-row mobile_container_${id}">
        <div class="col-md-7">
        <button type="button" class="fas fa-trash-alt" id="add_another_phone_no" onclick="deletePhone(${id})"></button>
        </div>
        <div class="col-md-5">
        <input type="radio" id="primary_phone_${id}"   name="primary_phone" value = ${id}>Primary
        </div>
        
    </div>
    

      <input  type="text" 
            name="phone_no[]" 
            id="phone_no_${id}" 
            class="form-control <?= $errors!='' && $errors->has('phone_no') ? 'error' : '';?>"
            placeholder = "phone_no"
            value=""/>
      <br>
        `
    );
    id++;
    
}

// function addEmail()
// {
//     $(".email_wrapper").append(
   
//         `
//         <div class="form-row email_container_${id}">
//         <div class="col-md-7">
//         <button type="button" class="fas fa-trash-alt"  onclick="deleteEmail(${id})"></button>
//         </div>
//         <div class="col-md-5">
//         <input type="radio" id="primary_email_${id}"   name="primary_email" value = ${id}>Primary
//         </div>
        
//     </div>

//         <input  type="text" 
//         name="email[]" 
//         id="email_${id}" 
//         class="form-control <?= $errors!='' && $errors->has('email') ? 'error' : '';?>"
//         placeholder = "email"
//         value=""/>
//         <br>
//         `
//     );
//     id++;

// }
function deletePhone(id)
{
    $(".mobile_container_"+id).remove();
    $("#phone_no_"+id).remove();
}
// function deleteEmail(id){
//     $(".email_container_"+id).remove();
//     $("#email_"+id).remove();
// }