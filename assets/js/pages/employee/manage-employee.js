var TableDataTables = function(){
    var handleEmployeeTable = function(){
        var manageEmployeeTable = $("#manage-employee-datatable");
        var baseURL = window.location.origin;
        var filePath = "/helper/routing.php";
        var oTable = manageEmployeeTable.dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: baseURL + filePath,
                method: "POST",
                data: {
                    "page": "manage_employee"
                }
            },
            "lengthMenu": [
                [5, 10, 20, -1],
                [5, 10, 20, "All"]
            ],
            "order": [
                [0, "ASC"]
            ],
            "columnDefs": [{
                'orderable': false,
                'targets': [0, -1]
            }],
        });
        
            manageEmployeeTable.on('click','.edit',function(){
                id = $(this).attr('id');
               
                address_id = $(this).attr('address_id');
                $("#employee_id").val(id);
                $("#address_id").val(address_id);
                $.ajax({
                    url: baseURL + filePath,
                    method: "POST",
                    data: {
                        "employee_id": id,
                        'address_id': address_id,
                        "fetch": "employee"
                    },
                    dataType: "json",
                });
            });
            manageEmployeeTable.on('click', '.delete', function(){
                id = $(this).attr('id');
                address_id = $(this).attr('address_id');
                $("#record_id").val(id);
                $("#address_id").val(address_id);
                $.ajax({
                    url: baseURL + filePath,
                    method: "POST",
                    data: {
                        "customer_id": id,
                        "fetch": "employee"
                    },
                    dataType: "json"
                    });
                });
                manageEmployeeTable.on('click','.view',function(){
                    id = $(this).attr('id');
                    address_id = $(this).attr('address_id');
                    $("#customer_id").val(id);
                    $("#address_id").val(address_id);
                    $.ajax({
                        url: baseURL + filePath,
                        method: "POST",
                        data: {
                            "employee_id": id,
                            "address_id": address_id,
                            "fetch": "employee"
                        },
                        dataType: "json"
                    });
                });
            }
            return{
                    init: function(){
                        handleEmployeeTable();
                    }
            }
}();
jQuery(document).ready(function(){
    TableDataTables.init();
})