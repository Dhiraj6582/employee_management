<?php

require_once __DIR__."/../helper/requirements.php";

class Employee{
    private $table = "employees";
    private $table1 = "addresses";
    private $table2 = "employee_mail";
    private $table3 = "employee_phoneno";
    private $table4 = "employee_whatsappno";
    private $database;
    protected $di;
    
    public function __construct(DependencyInjector $di)
    {
        $this->di = $di;
        $this->database = $this->di->get('database');
    }

    private function validateData($data)
    {
        $validator = $this->di->get('validator');
        return $validator->check($data, [
            'first_name' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 255,

            ],
            'last_name' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 255,
                
            ],
            'phone' => [
                'required' => true,
                'minlength' => 10,
                'maxlength' => 10,
                'unique' => $this->table
            ],
            'email' => [
                'required' => true,
                'unique' => $this->table
            ],
            'address' => [
                'required' => true,
                'minlength' => 10,
                'maxlength' => 10,                
            ],
            'access' => [
                'required' => true,
                'minlength' => 2,
                'maxlength' => 5
            ]
        ]);
    }

    public function addEmployee($data){
        $validation = $this->validateData($data);
        if(!$validation->fails()){
            try{
                $this->database->beginTransaction();
                $data_to_be_inserted = ['first_name' => $data['first_name'],
                                        'last_name' => $data['last_name']];
                $employee_id = $this->database->insert($this->table, $data_to_be_inserted);

                $address_to_be_inserted = [
                                        'address1'=> $data['address1'],
                                        'address2'=> $data['address2'],
                                        'location'=> $data['location'],
                                        'zipcode' => $data['zipcode'],
                                        'taluka' => $data['taluka'],
                                        'saburb' => $data['saburb'],
                                        'direction' => $data['direction'],
                                        'city' => $data['city'],
                                        'district' => $data['district'],
                                        'state' => $data['state'],
                                        'country' => $data['country'],
                                        'employee_id'=> $employee_id];
            
            
                $phone_no_to_be_inserted= [];
                $counter=1;
                foreach ($data['phone_no'] as $phone_no){
                    $phone_no_to_be_inserted = ['employee_id' => $employee_id,
                    'phone_no' =>  $phone_no];
                    if($data['primary_phone'] == $counter)
                    {
                        $phone_no_to_be_inserted['primary_phone']  =  1;
                    
                    }
                    else{
                        $phone_no_to_be_inserted['primary_phone']  =  0;
                    }
                $phone_id = $this->database->insert($this->table3, $phone_no_to_be_inserted);
                $counter++;
                }

               
                $whatapp_no_to_be_inserted=['whatsapp_no' => $data['whatsapp_no'],
                                            'employee_id'=> $employee_id];
                $mail_id_to_be_inserted=['mail' => $data['mail'],
                                        'employee_id'=> $employee_id];

                

                $address_id = $this->database->insert($this->table1, $address_to_be_inserted);
        
                $whatapp_id = $this->database->insert($this->table4, $whatapp_no_to_be_inserted);
                $mail_id = $this->database->insert($this->table2, $mail_id_to_be_inserted);                


                
            
                $this->database->commit();
                return ADD_SUCCESS;
             }
             catch (Exception $e){
                 $this->database->rollback();
                 return ADD_ERROR;
             }
        }
        else{
            return VALIDATION_ERROR;
        }
    }    
    public function getJSONDataForDataTable($draw, $searchParameter, $orderBy, $start, $length)
    {
        $columns = ["name","phone_no","mail","address"];
        $totalRowCountQuery = "SELECT COUNT(id) as total_count FROM {$this->table} ";
       // die(var_dump($totalRowCountQuery));
        $filteredRowCountQuery = "SELECT COUNT(id) as filtered_total_count FROM {$this->table}";
        $query = "Select employees.id, concat(first_name, ' ' , last_name) as 'name',addresses.id as 'address_id', concat(addresses.address1, ',',  addresses.city, ', ', addresses.zipcode) as 'address', employee_phoneno.phone_no, employee_mail.mail from employees INNER join addresses on employees.id = addresses.employee_id Inner Join employee_phoneno ON employees.id = employee_phoneno.employee_id INNER JOIN employee_mail ON employees.id = employee_mail.employee_id where primary_phone = 1 and primary_mail = 1";
        // Util::dd($query);
        if($searchParameter != null){
            $query .= " AND CONCAT(first_name, ' ' , last_name) LIKE '%$searchParameter%'";
        }

        if ($orderBy != null) {
            $query .= " ORDER BY {$columns[$orderBy[0]['column']]} {$orderBy[0]['dir']}";
        }
        else{
            $query .= " ORDER BY {$columns[0]} ASC";
        }		

        if($length != -1){
            $query .= " LIMIT {$start}, {$length}";
        }

        $totalRowCountResult = $this->database->raw($totalRowCountQuery);
        $numberOfTotalRows = is_array($totalRowCountResult) ? $totalRowCountResult[0]->total_count: 0;
        // Util::dd($filteredRowCountQuery);
        $filteredRowCountResult = $this->database->raw($filteredRowCountQuery);
        $numberOfFilteredRows = is_array($filteredRowCountResult) ? $filteredRowCountResult[0]->filtered_total_count: 0;

        $filteredData = $this->database->raw($query);
        $numberOfRowsToDisplay = is_array($filteredData) ? count($filteredData) : 0;
        // Util::dd($numberOfRowsToDisplay);
        $data = [];

        for($i = 0; $i<$numberOfRowsToDisplay; $i++){
            $subarray = [];
            $subarray[] = $filteredData[$i]->name;
            $subarray[] = $filteredData[$i]->phone_no;
            $subarray[] = $filteredData[$i]->mail;
            $subarray[] = $filteredData[$i]->address;
            $subarray[] = <<<BUTTONS
            <button class='edit btn btn-outline-primary' id='{$filteredData[$i]->id}' address_id={$filteredData[$i]->address_id}  data-toggle="modal" data-target="#editModal"><a href="edit-employee.php"><i class='fas fa-pencil-alt'></i></button>
            <button class='delete btn btn-outline-danger' id='{$filteredData[$i]->id}' data-toggle="modal" data-target="#deleteModal"><i class='fas fa-trash'></i></button>
BUTTONS;
            $data[] = $subarray;
        }
        
        $output = array(
            "draw" =>$draw,
            "recordsTotal" =>$numberOfTotalRows,
            "recordsFiltered" =>$numberOfFilteredRows,
            "data"=> $data
        );
        echo json_encode($output);
    }
    public function getEmployeeByid($employeeId, $mode=PDO::FETCH_OBJ){
        $query = "SELECT * FROM {$this->table}  where id = {$employeeId}";
        $result = $this->database->raw($query, $mode);
        return $result;
    }

    public function getAddressByid($addressId, $mode=PDO::FETCH_OBJ){
        $query = "SELECT * FROM {$this->table1}  where id = {$addressId}";
        $result = $this->database->raw($query, $mode);
        return $result;
    }
    public function getEmail($mailId, $mode=PDO::FETCH_OBJ){
        $query = "SELECT mail FROM {$this->table2} Inner Join {$this->table}  where employee_mail.employee_id = {$mailId}";
        $result = $this->database->raw($query, $mode);
        return $result;
    }
    public function getPrimaryPhoneNumber($phoneid, $mode=PDO::FETCH_OBJ){
        $query = "SELECT * FROM employee_phoneno  where employee_phoneno.employee_id = {$phoneid} and primary_phone = 1";
        $result = $this->database->raw($query, $mode);
        return $result;
    }
    public function getPhoneNumber($phoneid, $mode=PDO::FETCH_OBJ){
        $query = "SELECT * FROM employee_phoneno  where employee_phoneno.employee_id = {$phoneid} and primary_phone = 0";
        $result = $this->database->raw($query, $mode);
        return $result;
    }
    

    public function update($data, $id){
        $validationData['first_name'] = $data['first_name'];
        $validationData['last_name'] = $data['last_name'];
        
        $validation = $this->validateData($validationData);
        if(!$validation->fails()){
            try{
                $this->database->beginTransaction();
                $filteredData['first_name'] = $data['first_name'];
                $filteredData['last_name'] = $data['last_name'];
            $this->database->update($this->table, $filteredData, "id={$id}");
            $this->database->commit();
            return EDIT_SUCCESS;
            }
            catch(Exception $e){
                $this->database->rollback();
                return EDIT_ERROR;
            }   
        }else{
            return VALIDATION_ERROR;
        }
    }

    public function addressUpdate($data, $id)
    {
        $validationData['address1'] = $data['address1'];
        $validationData['address2'] = $data['address2'];
        $validationData['location'] = $data['location'];
        $validationData['address1'] = $data['address1'];
        $validationData['address2'] = $data['address2'];
        $validationData['location'] = $data['location'];
        $validationData['zipcode'] = $data['zipcode'];
        $validationData['taluka'] = $data['taluka'];
        $validationData['saburb'] = $data['saburb'];
        $validationData['direction'] = $data['direction'];
        $validationData['city'] = $data['city'];
        $validationData['district'] = $data['district'];
        $validationData['state'] = $data['state'];
        $validationData['country'] = $data['country'];

        $validation = $this->validateData($validationData);
        if(!$validation->fails()){
            try{
                $this->database->beginTransaction();
                $validationData['address1'] = $data['address1'];
                $validationData['address2'] = $data['address2'];
                $validationData['location'] = $data['location'];
                $filteredData['address1'] = $data['address1'];
                $filteredData['address2'] = $data['address2'];
                $filteredData['location'] = $data['location'];
                $filteredData['zipcode'] = $data['zipcode'];
                $filteredData['taluka'] = $data['taluka'];
                $filteredData['saburb'] = $data['saburb'];
                $filteredData['direction'] = $data['direction'];
                $filteredData['city'] = $data['city'];
                $filteredData['district'] = $data['district'];
                $filteredData['state'] = $data['state'];
                $filteredData['country'] = $data['country'];

                $this->database->update($this->table1, $filteredData, "id={$id}");
                $this->database->commit();
                return EDIT_SUCCESS;
            }
            catch(Exception $e){
                $this->database->rollback();
                return EDIT_ERROR;
            }   
        }
        else{
            return VALIDATION_ERROR;
        }
        
    }
    public function phoneUpdate($data, $id)
    {
        //Util::dd($data);
        $validationData['phone_no'] = $data['phone_no'];
        $validation = $this->validateData($validationData);
        if(!$validation->fails()){
            try{
                $this->database->beginTransaction();
                
                $counter = 1;
                foreach($data['phone_no'] as $phone_no)
                {    
                    $data_to_be_inserted = [
                        'employee_id'=> $id,
                        'phone_no'=> $phone_no
                    ];
                    if($data['primary_phone'] == $counter)
                    {
                        $data_to_be_inserted['primary_phone'] = 1;
                    }
                    else
                    {
                        $data_to_be_inserted['primary_phone'] = 0;
                    }
                    $this->database->insert($this->table3, $data_to_be_inserted);
                
                $counter++;
                }

                
            $this->database->commit();
            return EDIT_SUCCESS;
            }
            catch(Exception $e){
                $this->database->rollback();
                return EDIT_ERROR;
            }   
        }else{
            return VALIDATION_ERROR;
        }
    }

    
    public function delete($id)
    {
            try{
                    $this->database->beginTransaction();
                    $this->database->delete($this->table,"id={$id}");
                    $this->database->commit();
                    return DELETE_SUCCESS;
                }catch(Exception $e){
                    $this->database->rollback();
                    return DELETE_ERROR;
            }
    }
}