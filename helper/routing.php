<?php
require_once 'init.php';


if(isset($_POST['add_employee']))
{
    //USER HAS REQUESTED TO ADD A NEW CUSTOMER
    if(isset($_POST['csrf_token']) && Util::verifyCSRFToken($_POST))
    {
        //Util::dd($_POST);
        $result = $di->get('employee')->addEmployee($_POST);   
        switch($result)
        {
            case ADD_ERROR:
                Session::setSession(ADD_ERROR, 'There was problem while inserting record, please try again later!');
                Util::redirect('manage-employee.php');
                break;
            case ADD_SUCCESS:
                Session::setSession(ADD_SUCCESS, 'The record have been added successfully!');
                // Util::dd();
                Util::redirect('manage-employee.php');
                break;
            case VALIDATION_ERROR:
                Session::setSession('errors', serialize($di->get('validator')->errors()));
                Session::setSession('old', $_POST);
                Util::redirect('add-employee.php');
                break;
        }
    }
}
if(isset($_POST['page']) && $_POST['page'] == 'manage_employee')
{
  
    $search_parameter = $_POST['search']['value'] ?? null;
    $order_by = $_POST['order'] ?? null;
    $start = $_POST['start'];
    $length = $_POST['length'];
    $draw = $_POST['draw'];
    $di->get("employee")->getJSONDataForDataTable($draw,$search_parameter,$order_by,$start,$length);
}

if(isset($_POST['fetch']) && $_POST['fetch'] =='employee')
{
    $employee_id = $_POST['employee_id'];
    $address_id = $_POST['address_id'];
    $result = $di->get('employee')->getEmployeeByid($employee_id,PDO::FETCH_ASSOC);
    $result1 = $di->get('employee')->getAddressByid($address_id,PDO::FETCH_ASSOC);
    $result2 = $di->get('employee')->getEmail($employee_id, PDO::FETCH_ASSOC);
    $result3 = $di->get('employee')->getPrimaryPhoneNumber($employee_id, PDO::FETCH_ASSOC);
    $result4 = $di->get('employee')->getPhoneNumber($employee_id, PDO::FETCH_ASSOC);
    Session::setSession('old_values', $result);
    Session::setSession('old_address', $result1);
    Session::setSession('old_email', $result2);
    Session::setSession('old_primary_phone_no', $result3);
    Session::setSession('old_phone_no', $result4);
  

    echo json_encode($result4);
}
if(isset($_POST['edit_employee']))
{
    if(isset($_POST['csrf_token']) && Util::verifyCSRFToken($_POST))
    {
       // Util::dd($_POST);
        $employee_id = $_POST['employee_id'];
        $result = $di->get('employee')->update($_POST,$_POST['employee_id']);
        $result1 = $di->get('employee')->addressUpdate($_POST,$_POST['address_id']);
        $database->delete('employee_phoneno', "employee_id={$employee_id}");
        $result2 = $di->get('employee')->phoneUpdate($_POST,$_POST['employee_id']);
        switch($result)
        {
            case EDIT_ERROR:
                Session::setSession(ADD_ERROR, 'There was problem while editing record, please try again later!');
                Util::redirect('manage-employee.php');
                break;
            case EDIT_SUCCESS:
                Session::setSession(ADD_SUCCESS, 'The record have been updated successfully!');
                Util::redirect('manage-employee.php');
                break;
            case VALIDATION_ERROR:
                    Session::setSession('errors', serialize($di->get('validator')->errors()));
                    Session::setSession('old', $_POST);
                    Util::redirect('add-employee.php');
                    break;
        }
        switch($result1)
        {
            case EDIT_ERROR:
                Session::setSession(ADD_ERROR, 'There was problem while editing record, please try again later!');
                Util::redirect('manage-employee.php');
                break;
            case EDIT_SUCCESS:
                Session::setSession(ADD_SUCCESS, 'The record have been updated successfully!');
                Util::redirect('manage-employee.php');
                break;
            case VALIDATION_ERROR:
                    Session::setSession('errors', serialize($di->get('validator')->errors()));
                    Session::setSession('old', $_POST);
                    Util::redirect('add-employee.php');
                    break;
        }
        switch($result2)
        {
            case EDIT_ERROR:
                Session::setSession(ADD_ERROR, 'There was problem while editing record, please try again later!');
                Util::redirect('manage-employee.php');
                break;
            case EDIT_SUCCESS:
                Session::setSession(ADD_SUCCESS, 'The record have been updated successfully!');
                Util::redirect('manage-employee.php');
                break;
            case VALIDATION_ERROR:
                    Session::setSession('errors', serialize($di->get('validator')->errors()));
                    Session::setSession('old', $_POST);
                    Util::redirect('add-employee.php');
                    break;
        }
    }
}
if(isset($_POST['delete_employee']))
{
    if(isset($_POST['csrf_token']) && Util::verifyCSRFToken($_POST))
    {
        $result = $di->get('employee')->delete($_POST['record_id']);
        $result1 = $di->get('address')->delete($_POST['address_id']);
        switch($result)
        {
            case DELETE_ERROR:
                Session::setSession(DELETE_ERROR, 'There was problem while deleting  record, please try again later!');
                Util::redirect('manage-employee.php');
                break;
            case DELETE_SUCCESS:
                Session::setSession(DELETE_SUCCESS, 'The record have been deleted successfully!');
                // Util::dd();
                Util::redirect('manage-employee.php');
                break;
        }
        switch($result1)
        {
            case DELETE_ERROR:
                Session::setSession(DELETE_ERROR, 'There was problem while deleting  record, please try again later!');
                Util::redirect('manage-employee.php');
                break;
            case DELETE_SUCCESS:
                 Session::setSession(DELETE_SUCCESS, 'The record have been deleted successfully!');
                // Util::dd();
                Util::redirect('manage-employee.php');
                break;
        }
    }
}
