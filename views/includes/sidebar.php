<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">EASY ERP</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="index.php">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Interface
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item <?= $sidebarSection == 'category' ? 'active' : '';?>">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseCategory" aria-expanded="true" aria-controls="collapseCategory">
          <i class="fas fa-fw fa-cog"></i>
          <span>Category</span>
        </a>
        <div id="collapseCategory" class="collapse <?= $sidebarSection == 'category' ? 'show' : ''; ?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item <?= $sidebarSection == 'category' && $sidebarSubSection == 'manage' ? 'active' : ''; ?>" href="<?=BASEPAGES;?>manage-category.php">Manage Category</a>
            <a class="collapse-item <?= $sidebarSection == 'category' && $sidebarSubSection == 'add' ? 'active' : ''; ?>" href="<?=BASEPAGES;?>add-category.php">Add Category</a>
          </div>
        </div>
      </li>

      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item <?= $sidebarSection == 'customer' ? 'active' : '';?>">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseCustomer" aria-expanded="true" aria-controls="collapseCustomer">
          <i class="fas fa-fw fa-cog"></i>
          <span>Customers</span>
        </a>
        <div id="collapseCustomer" class="collapse <?= $sidebarSection == 'customer' ? 'show' : ''; ?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item <?= $sidebarSection == 'customer' && $sidebarSubSection == 'manage' ? 'active' : ''; ?>" href="<?=BASEPAGES;?>manage-customer.php">Manage Customer</a>
            <a class="collapse-item <?= $sidebarSection == 'customer' && $sidebarSubSection == 'add' ? 'active' : ''; ?>" href="<?=BASEPAGES;?>add-customer.php">Add Customer</a>
          </div>
        </div>
      </li>

      <li class="nav-item <?= $sidebarSection == 'employee' ? 'active' : '';?>">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseEmployees" aria-expanded="true" aria-controls="collapseEmployees">
          <i class="fas fa-fw fa-cog"></i>
          <span>Employees</span>
        </a>
        <div id="collapseEmployees" class="collapse <?= $sidebarSection == 'employee' ? 'show' : ''; ?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item <?= $sidebarSection == 'employee' && $sidebarSubSection == 'manage' ? 'active' : ''; ?>" href="<?=BASEPAGES;?>manage-employee.php">Manage Employees</a>
            <a class="collapse-item <?= $sidebarSection == 'employee' && $sidebarSubSection == 'add' ? 'active' : ''; ?>" href="<?=BASEPAGES;?>add-employee.php">Add Employees</a>
          </div>
        </div>
      </li>

      <li class="nav-item <?= $sidebarSection == 'products' ? 'active' : '';?>">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseProducts" aria-expanded="true" aria-controls="collapseProducts">
          <i class="fas fa-fw fa-cog"></i>
          <span>Products</span>
        </a>
        <div id="collapseProducts" class="collapse <?= $sidebarSection == 'product' ? 'show' : ''; ?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item <?= $sidebarSection == 'manage' ? 'active' : ''; ?>" href="<?=BASEPAGES;?>manage-product.php">Manage Products</a>
            <a class="collapse-item <?= $sidebarSection == 'manage' ? 'active' : ''; ?>" href="#">Check Current Activity</a>
            <a class="collapse-item <?= $sidebarSection == 'add' ? 'active' : ''; ?>" href="<?=BASEPAGES;?>add-product.php">Add Product</a>
          </div>
        </div>
      </li>
      <li class="nav-item <?= $sidebarSection == 'supplier' ? 'active' : '';?>">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseSuppliers" aria-expanded="true" aria-controls="collapseProducts">
          <i class="fas fa-fw fa-cog"></i>
          <span>Supplier</span>
        </a>
        <div id="collapseSuppliers" class="collapse <?= $sidebarSection == 'supplier' ? 'show' : ''; ?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item <?=  $sidebarSection == 'supplier' && $sidebarSubSection == 'manage' ? 'active' : ''; ?>" href="<?=BASEPAGES;?>manage-supplier.php">Manage Suppliers</a>
            <a class="collapse-item <?=  $sidebarSection == 'supplier' && $sidebarSubSection == 'add' ? 'active' : ''; ?>" href="<?=BASEPAGES;?>add-supplier.php">Add Supplier</a>
          </div>
        </div>
      </li>

      <li class="nav-item <?= $sidebarSection == 'transaction' ? 'show' : '';?>">
        <a class="nav-link collapsed <?= $sidebarSection == 'transaction' ?'': 'collapsed';?>" href="#" data-toggle="collapse" data-target="#collapseTransaction" aria-expanded="true" aria-controls="collapseTransaction">
          <i class="fas fa-fw fa-cog"></i>
          <span>Transaction</span>
        </a>
        <div id="collapseTransaction" class="collapse <?= $sidebarSection == 'transaction' ? 'show' : ''; ?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item <?=  $sidebarSection == 'transaction' && $sidebarSubSection == 'purchase' ? 'active' : ''; ?>" href="<?=BASEPAGES;?>add-purchase.php">Purchase</a>
            <a class="collapse-item <?=  $sidebarSection == 'transaction' && $sidebarSubSection == 'sales' ? 'active' : ''; ?>" href="<?=BASEPAGES;?>add-sales.php">Sale</a>
          </div>
        </div>
      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Addons
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-folder"></i>
          <span>Reports</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Login Screens:</h6>
            <a class="collapse-item" href="login.html">Purchase History</a>
            <a class="collapse-item" href="register.html">Sales Report</a>
            <a class="collapse-item" href="forgot-password.html">P &amp; L Statement</a>
          </div>
        </div>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>