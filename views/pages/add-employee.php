<?php
require_once __DIR__ . '/../../helper/init.php';
$pageTitle = "Easy ERP | Add Employee";
$sidebarSection = "employee";
$sidebarSubSection = "add";
Util::createCSRFToken();
$errors = "";
if(Session::hasSession('errors'))
{
  $errors = unserialize(Session::getSession('errors'));
  Session::unsetSession('errors');
}
$old = "";
if(Session::hasSession('old'))
{
  $old = Session::getSession('old');
  Session::unsetSession('old');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
<?php
  require_once __DIR__ . "/../includes/head-section.php";
  ?>

</head>

<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
        <div class="container-fluid">

          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Category</h1>
            <a href="<?= BASEPAGES; ?>manage-customer.php" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
              <i class="fa fa-list-ul fa-sm text-white-75"></i> Manage Customer
            </a>
          </div>

          <div class="row">

            <div class="col-lg-12">

              <!-- Basic Card Example -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Add Employee</h6>
                </div>
                <div class="card-body">
                  <div class="col-md-12">

                    <form action="<?=BASEURL;?>helper/routing.php" method="POST" id="add-employee">
                      <input type="hidden" name="csrf_token" value="<?= Session::getSession('csrf_token');?>">
                      <!--FORM GROUP-->
                      <div class="form-row">
                            <div class="form-group col-md-12">
                            Employee Name
                            </div>
                            <div class="form-group col-md-6">
                            <label for="first_name">First Name</label>
                                      <input  type="text" 
                                            name="first_name" 
                                            id="first_name" 
                                            class="form-control <?= $errors!='' && $errors->has('first_name') ? 'error' : '';?>"
                                            placeholder = "Enter First Name"
                                            value="<?=$old != '' && isset($old['first_name']) ?$old['first_name']: '';?>"/>
                                      <?php
                                    if($errors!="" && $errors->has('first_name'))
                                    {
                                        echo "<span class='error'>{$errors->first('first_name')}</span>";
                                    }
                                    ?>
                            </div>
                            <div class="form-group col-md-6">
                                      <label for="last_name">Last Name</label>
                                      <input type="text" 
                                            name="last_name" 
                                            id="last_name"
                                            class="form-control <?= $errors!='' && $errors->has('last_name') ? 'error' : '';?>"
                                            placeholder = "Enter Last Name"
                                            value="<?=$old != '' && isset($old['last_name']) ?$old['last_name']: '';?>"/>
                                      <?php
                                      if($errors!="" && $errors->has('last_name'))
                                      {
                                          echo "<span class='error'>{$errors->first('last_name')}</span>";
                                      }
                                      ?>
                                </div>
                      </div>

                      <div class="form-row">
                            <div class="form-group col-md-12 ">Address Details</div>
                                <div class="form-group col-md-4">
                                      <label for="address_name">Address 1*</label>
                                      <input  type="text" 
                                            name="address1" 
                                            id="address1" 
                                            class="form-control <?= $errors!='' && $errors->has('address1') ? 'error' : '';?>"
                                            placeholder = "Address 1"
                                            value="<?=$old != '' && isset($old['address1']) ?$old['address1']: '';?>"/>
                                      <?php
                                    if($errors!="" && $errors->has('address1'))
                                    {
                                        echo "<span class='error'>{$errors->first('address1')}</span>";
                                    }
                                    ?>

                                </div>

                                 <div class="form-group col-md-4">
                                      <label for="address_name">Address 2</label>
                                      <input  type="text" 
                                            name="address2" 
                                            id="address2" 
                                            class="form-control <?= $errors!='' && $errors->has('address2') ? 'error' : '';?>"
                                            placeholder = "Address 2"
                                            value="<?=$old != '' && isset($old['address2']) ?$old['address2']: '';?>"/>
                                      <?php
                                    if($errors!="" && $errors->has('address2'))
                                    {
                                        echo "<span class='error'>{$errors->first('address2')}</span>";
                                    }
                                    ?>
                                  </div>
                                <div class="form-group col-md-4">
                                      <label for="address_name">Location</label>
                                      <input  type="text" 
                                            name="location" 
                                            id="location" 
                                            class="form-control <?= $errors!='' && $errors->has('location') ? 'error' : '';?>"
                                            placeholder = "Location"
                                            value="<?=$old != '' && isset($old['location']) ?$old['location']: '';?>"/>
                                      <?php
                                    if($errors!="" && $errors->has('location'))
                                    {
                                        echo "<span class='error'>{$errors->first('location')}</span>";
                                    }
                                    ?>
                                  </div>

                                
                               
                                
                            
                          </div>

                          <div class="form-row">
                                <div class="form-group col-md-4">
                                      <label for="address_name">Zip /Postal Code</label>
                                      <input  type="text" 
                                            name="zipcode" 
                                            id="zipcode" 
                                            class="form-control <?= $errors!='' && $errors->has('zipcode') ? 'error' : '';?>"
                                            placeholder = "Zip Code"
                                            value="<?=$old != '' && isset($old['zipcode']) ?$old['zipcode']: '';?>"/>
                                      <?php
                                    if($errors!="" && $errors->has('zipcode'))
                                    {
                                        echo "<span class='error'>{$errors->first('zipcode')}</span>";
                                    }
                                    ?>

                                </div>

                                 <div class="form-group col-md-4">
                                      <label for="address_name">Postal Area</label>
                                      <input  type="text" 
                                            name="postal_code" 
                                            id="postal_code" 
                                            class="form-control <?= $errors!='' && $errors->has('postal_code') ? 'error' : '';?>"
                                            placeholder = "Postal area"
                                            value="<?=$old != '' && isset($old['postal_code']) ?$old['postal_code']: '';?>"/>
                                      <?php
                                    if($errors!="" && $errors->has('postal_code'))
                                    {
                                        echo "<span class='error'>{$errors->first('postal_code')}</span>";
                                    }
                                    ?>
                                  </div>
                                <div class="form-group col-md-4">
                                      <label for="address_name">Taluka</label>
                                      <input  type="text" 
                                            name="taluka" 
                                            id="taluka" 
                                            class="form-control <?= $errors!='' && $errors->has('taluka') ? 'error' : '';?>"
                                            placeholder = "Taluka"
                                            value="<?=$old != '' && isset($old['taluka']) ?$old['taluka']: '';?>"/>
                                      <?php
                                    if($errors!="" && $errors->has('taluka'))
                                    {
                                        echo "<span class='error'>{$errors->first('taluka')}</span>";
                                    }
                                    ?>
                                  </div>  
                          </div>

                          <div class="form-row">
                                <div class="form-group col-md-4">
                                      <label for="address_name">Sub Urban</label>
                                      <input  type="text" 
                                            name="saburb" 
                                            id="saburb" 
                                            class="form-control <?= $errors!='' && $errors->has('saburb') ? 'error' : '';?>"
                                            placeholder = "saburb"
                                            value="<?=$old != '' && isset($old['saburb']) ?$old['saburb']: '';?>"/>
                                      <?php
                                    if($errors!="" && $errors->has('saburb'))
                                    {
                                        echo "<span class='error'>{$errors->first('saburb')}</span>";
                                    }
                                    ?>

                                </div>

                                 <div class="form-group col-md-4">
                                      <label for="address_name">Direction</label>
      `````                           <select name="direction" class="form-control">
                                        <option selected>Choose...</option>
                                        <option value="East">East</option>
                                        <option value="West">West</option>
                                        </select>
                                  
                                  </div>
                                <div class="form-group col-md-4">
                                      <label for="address_name">City</label>
                                      <select name="city" class="form-control">
                                        <option selected>Select...</option>
                                        <option value="India">India</option>
                                        </select>
                                  </div>

                                
                               
                                
                            
                          </div>
                          
                          <div class="form-row">
                                <div class="form-group col-md-4">
                                      <label for="address_name">District</label>
                                      <input  type="text" 
                                            name="district" 
                                            id="district" 
                                            class="form-control <?= $errors!='' && $errors->has('district') ? 'error' : '';?>"
                                            placeholder = "district"
                                            value="<?=$old != '' && isset($old['district']) ?$old['district']: '';?>"/>
                                      <?php
                                    if($errors!="" && $errors->has('district'))
                                    {
                                        echo "<span class='error'>{$errors->first('district')}</span>";
                                    }
                                    ?>

                                </div>

                                 <div class="form-group col-md-4">
                                      <label for="address_name">State</label>
                                      <select name="state" class="form-control">
                                          <option selected>Select...</option>
                                          <option value="Maharashtra">Maharashtra</option>
                                          <option value="Punjab">Punjab</option>
                                          <option value="West Bengal">Gujrat</option>
                                          <option value="West Bengal">West Bengal</option>
                                          </select>
                                  </div>
                                <div class="form-group col-md-4">
                                      <label for="address_name">Country</label>
                                      <input  type="text" 
                                            name="country" 
                                            id="country" 
                                            class="form-control <?= $errors!='' && $errors->has('country') ? 'error' : '';?>"
                                            placeholder = "COuntry"
                                            value="<?=$old != '' && isset($old['country']) ?$old['country']: '';?>"/>
                                      <?php
                                    if($errors!="" && $errors->has('country'))
                                    {
                                        echo "<span class='error'>{$errors->first('country')}</span>";
                                    }
                                    ?>
                                  </div>
                          </div>

                          <div class="form-row">
                            <div class="form-group col-md-12 ">Contact Details</div>
                                <div class="form-group col-md-4">
                                <div class="form-row">
                                        <div class="col-md-11">
                                        <label for="address_name">Phone Number</label>  
                                        </div>
                                        <div class="col-md-1">
                                        <button type="button" class="fas fa-plus-square" id="add_another_phone_no" onclick="addPhone()"></button>
                                        </div>
                                        
                                    </div>
                                    
                                    <div class="form-row mobile_container_1">
                                        <div class="col-md-7">
                                        <label for="address_name">Mobile Number</label>  
                                        </div>
                                        <div class="col-md-5 ">
                                        <input type="radio"  class="radio_select "id="primary_phone_1" name="primary_phone" value = 1>Primary
                                        </div>
                                        
                                    </div>
                                    
                              
                              
                                      <input  type="text" 
                                            name="phone_no[]" 
                                            id="phone_no_1" 
                                            class="form-control <?= $errors!='' && $errors->has('phone_no') ? 'error' : '';?>"
                                            placeholder = "phone_no"
                                            value="0"/>
                                      <br>
                                    <div class="phone_wrapper"></div>
                                      
                                            
                                    

                                </div>
                                
                                <div class="form-group col-md-4">
                                    <div class="form-row">
                                        <div class="col-md-11">
                                        <label for="address_name">What App Number</label>  
                                        </div>
                                        <div class="col-md-1">
                                        <button  class="fas fa-plus-square" id="add_another_whatapp_no"></button> -->
                                        </div>
                                        <div class="whatapp_wrapper col-md-12"></div>
                                    </div>
                                      
                                      <input  type="text" 
                                            name="whatsapp_no" 
                                            id="whatsapp_no" 
                                            class="form-control <?= $errors!='' && $errors->has('whatapp_no') ? 'error' : '';?>"
                                            placeholder = "whatsapp_no"
                                            value="<?=$old != '' && isset($old['whatsapp_no']) ?$old['whatapps_no']: '';?>"/>
                                      <?php
                                    if($errors!="" && $errors->has('whatapps_no'))
                                    {
                                        echo "<span class='error'>{$errors->first('whatsapp_no')}</span>";
                                    }
                                    ?>
                                  </div>
                                <div class="form-group col-md-4">
                                <div class="form-row">
                                        <div class="col-md-11">
                                        <label for="address_name">Email</label>  
                                        </div>
                                        <div class="col-md-1">
                                             <button  class="fas fa-plus-square" id="add_another_Email"></button> 
                                        </div>
                                        <div class="email_wrapper col-md-12"></div>
                                    </div>
                                      <input  type="text" 
                                            name="mail" 
                                            id="mail" 
                                            class="form-control <?= $errors!='' && $errors->has('mail') ? 'error' : '';?>"
                                            placeholder = "mail"
                                            value="<?=$old != '' && isset($old['mail']) ?$old['mail']: '';?>"/>
                                      <?php
                                    if($errors!="" && $errors->has('mail'))
                                    {
                                        echo "<span class='error'>{$errors->first('mail')}</span>";
                                    }
                                    ?>
                                  </div>
                               
                          
                          </div>  








                  
                                
                               
                                
                          
                      <!--/FORM GROUP-->
                      <button type="submit" class="btn btn-primary" name="add_employee" value="addEmployee"><i class="fa fa-check"></i> Submit</button>
                    </form>

                             

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->
      <!-- Footer -->
      <?php require_once(__DIR__ . "/../includes/footer.php"); ?>
      <!-- End of Footer -->
    </div>
    <!-- End of Content Wrapper -->
  </div>
  <!-- End of Page Wrapper -->
  <?php
  require_once(__DIR__ . "/../includes/scroll-to-top.php");
  ?>
  <?php require_once(__DIR__ . "/../includes/core-scripts.php"); ?>
  
  <!--PAGE LEVEL SCRIPTS-->
  <?php require_once(__DIR__ . "/../includes/page-level/employee/add-employee-scripts.php");?>
</body>

</html>