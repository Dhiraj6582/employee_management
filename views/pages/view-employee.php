<?php
require_once __DIR__ . '/../../helper/init.php';
$pageTitle = "Easy ERP | Add Employee";
$sidebarSection = "employee";
$sidebarSubSection = "add";
Util::createCSRFToken();
$errors = "";
if(Session::hasSession('errors'))
{
  $errors = unserialize(Session::getSession('errors'));
  Session::unsetSession('errors');
}
$old_values = "";
if(Session::hasSession('old_values'))
{
  $old_values = Session::getSession('old_values');
}
$old_address = "";
if(Session::hasSession('old_address'))
{
  // Util::dd(Session::getSession('old_address'));
  $old_address = Session::getSession('old_address');
}
// Util::dd($_SESSION);
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <?php
  require_once __DIR__ . "/../includes/head-section.php";
  ?>

  <!--PLACE TO ADD YOUR CUSTOM CSS-->

</head>

<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    <?php require_once(__DIR__ . "/../includes/sidebar.php"); ?>
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        <?php require_once(__DIR__ . "/../includes/navbar.php"); ?>
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Employee</h1>
            <a href="<?= BASEPAGES; ?>manage-employee.php" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
              <i class="fa fa-list-ul fa-sm text-white-75"></i> Manage Employees
            </a>
          </div>

          <div class="row">

            <div class="col-lg-12">

              <!-- Basic Card Example -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">View Employee</h6>
                </div>
                <div class="card-body">
                  <div class="col-md-12">

                      <!--FORM GROUP-->
                      <div class="form-group">
                      <input type="hidden" name="csrf_token" value="<?= Session::getSession('csrf_token');?>">
                        <label for="first_name">Employee's First Name</label>
                        <input  type="text" 
                                name="first_name" 
                                id="first_name" 
                                class="form-control <?= $errors!='' && $errors->has('first_name') ? 'error' : '';?>"
                                value="<?=$old_values != '' && isset($old_values[0]['first_name']) ?$old_values[0]['first_name']: '';?>"
                                disabled="true"/>
                        <?php
                          if($errors!="" && $errors->has('first_name'))
                          {
                            echo "<span class='error'>{$errors->first('first_name')}</span>";
                          }
                        ?>
                        <br>
                        <label for="last_name">Employee's Last Name</label>
                        <input  type="text" 
                                name="last_name" 
                                id="last_name" 
                                class="form-control <?= $errors!='' && $errors->has('last_name') ? 'error' : '';?>"
                                value="<?=$old_values != '' && isset($old_values[0]['last_name']) ?$old_values[0]['last_name']: '';?>"
                                disabled="true"/>
                        <?php
                          if($errors!="" && $errors->has('last_name'))
                          {
                            echo "<span class='error'>{$errors->first('last_name')}</span>";
                          }
                        ?>
                        <br>
                        <label for="phone_no">Employee's Phone number</label>
                        <input  type="text" 
                                name="phone_no" 
                                id="phone_no" 
                                class="form-control <?= $errors!='' && $errors->has('phone_no') ? 'error' : '';?>"
                                value="<?=$old_values != '' && isset($old_values[0]['phone_no']) ?$old_values[0]['phone_no']: '';?>"
                                disabled="true"/>
                        <?php
                          if($errors!="" && $errors->has('phone_no'))
                          {
                            echo "<span class='error'>{$errors->first('phone_no')}</span>";
                          }
                        ?>
                        <br>
                        <label for="email_id">Employee's Email</label>
                        <input  type="email_id" 
                                name="email_id" 
                                id="email_id" 
                                class="form-control <?= $errors!='' && $errors->has('email_id') ? 'error' : '';?>"
                                value="<?=$old_values != '' && isset($old_values[0]['email_id']) ?$old_values[0]['email_id']: '';?>"
                                disabled="true"/>
                        <?php
                          if($errors!="" && $errors->has('email'))
                          {
                            echo "<span class='error'>{$errors->first('email_id')}</span>";
                          }
                        ?>
                         <br>
                        <label for="gender">Employee's Gender</label>
                        <input  type="text" 
                                name="gender" 
                                id="gender" 
                                class="form-control <?= $errors!='' && $errors->has('gender') ? 'error' : '';?>"
                                value="<?=$old_values != '' && isset($old_values[0]['gender']) ?$old_values[0]['gender']: '';?>"
                                disabled="true"/>
                        <?php
                          if($errors!="" && $errors->has('gender'))
                          {
                            echo "<span class='error'>{$errors->first('gender')}</span>";
                          }
                          ?>
                        <label for="block_no">Block No.</label>
                        <input  type="text" 
                                name="block_no" 
                                id="block_no" 
                                class="form-control <?= $errors!='' && $errors->has('block_no') ? 'error' : '';?>"
                                value="<?=$old_address != '' && isset($old_address[0]['block_no']) ?$old_address[0]['block_no']: '';?>"
                                disabled="true"/>
                        <?php
                          if($errors!="" && $errors->has('block_no'))
                          {
                            echo "<span class='error'>{$errors->first('block_no')}</span>";
                          }
                          ?>
                          
                        <label for="street">Street</label>
                        <input  type="text" 
                                name="street" 
                                id="street" 
                                class="form-control <?= $errors!='' && $errors->has('street') ? 'error' : '';?>"
                                value="<?=$old_address != '' && isset($old_address[0]['street']) ?$old_address[0]['street']: '';?>"
                                disabled="true"/>
                        <?php
                          if($errors!="" && $errors->has('street'))
                          {
                            echo "<span class='error'>{$errors->first('street')}</span>";
                          }
                          ?>
                          
                        <label for="city">City</label>
                        <input  type="text" 
                                name="city" 
                                id="city" 
                                class="form-control <?= $errors!='' && $errors->has('city') ? 'error' : '';?>"
                                value="<?=$old_address != '' && isset($old_address[0]['city']) ?$old_address[0]['city']: '';?>"
                                disabled="true"/>
                        <?php
                          if($errors!="" && $errors->has('city'))
                          {
                            echo "<span class='error'>{$errors->first('city')}</span>";
                          }
                          ?>
                          
                        <label for="pincode">Pincode</label>
                        <input  type="text" 
                                name="pincode" 
                                id="pincode" 
                                class="form-control <?= $errors!='' && $errors->has('pincode') ? 'error' : '';?>"
                                value="<?=$old_address != '' && isset($old_address[0]['pincode']) ?$old_address[0]['pincode']: '';?>"
                                disabled="true"/>
                        <?php
                          if($errors!="" && $errors->has('pincode'))
                          {
                            echo "<span class='error'>{$errors->first('pincode')}</span>";
                          }
                          ?>
                          
                        <label for="state">State</label>
                        <input  type="text" 
                                name="state" 
                                id="state" 
                                class="form-control <?= $errors!='' && $errors->has('state') ? 'error' : '';?>"
                                value="<?=$old_address != '' && isset($old_address[0]['state']) ?$old_address[0]['state']: '';?>"
                                disabled="true"/>
                        <?php
                          if($errors!="" && $errors->has('state'))
                          {
                            echo "<span class='error'>{$errors->first('state')}</span>";
                          }
                          ?>
                          
                        <label for="country">Country</label>
                        <input  type="text" 
                                name="country" 
                                id="country" 
                                class="form-control <?= $errors!='' && $errors->has('country') ? 'error' : '';?>"
                                value="<?=$old_address != '' && isset($old_address[0]['country']) ?$old_address[0]['country']: '';?>"
                                disabled="true"/>
                        <?php
                          if($errors!="" && $errors->has('country'))
                          {
                            echo "<span class='error'>{$errors->first('country')}</span>";
                          }
                          ?>
                        <label for="town">Town</label>
                        <input  type="text" 
                                name="town" 
                                id="town" 
                                class="form-control <?= $errors!='' && $errors->has('town') ? 'error' : '';?>"
                                value="<?=$old_address != '' && isset($old_address[0]['town']) ?$old_address[0]['town']: '';?>"
                                disabled="true"/>
                        <?php
                          if($errors!="" && $errors->has('town'))
                          {
                            echo "<span class='error'>{$errors->first('town')}</span>";
                          }
                          ?>
                      </div>
                      <!--/FORM GROUP-->
                      <button class="btn btn-outline-primary"><a href="edit-employee.php"><i class="fas fa-pencil-alt"></i> Edit</a></button>
                      <button class="delete btn btn-outline-danger" data-toggle="modal" data-target="#deleteModal"><i class="fas fa-trash"></i> Delete</button>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->
      <!-- Footer -->
      <?php require_once(__DIR__ . "/../includes/footer.php"); ?>
      <!-- End of Footer -->
    </div>
    <!-- End of Content Wrapper -->
  </div>
  <!-- End of Page Wrapper -->
   <!--DELETE MODAL-->
   <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="deleteModalLabel">Delete?</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="<?=BASEURL;?>helper/routing.php" method="POST">
          <div class="modal-body">
          <input type="hidden" name="csrf_token" id="csrf_token" value="<?= Session::getSession('csrf_token');?>">
            <input type="hidden" name="record_id" id="record_id" value=<?=$old_values[0]['id']?>>
            <input type="hidden" name="address_id" id="address_id" value=<?=$old_address[0]['id']?>>
            <p>Are you sure you want to delete this record?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-danger" name="delete_employee">Delete</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!--/DELETE MODAL-->
  <?php
  require_once(__DIR__ . "/../includes/scroll-to-top.php");
  ?>
  <?php require_once(__DIR__ . "/../includes/core-scripts.php"); ?>
  
  <!--PAGE LEVEL SCRIPTS-->
</body>

</html>